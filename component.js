const React = require('react');

const MyComponent = React.createClass({
  propTypes: {

  },

  getDefaultProps() {

  },

  getInitialState() {

  },

  componentWillMount() {

  },

  componentDidMount() {

  },

  componentWillReceiveProps() {

  },

  shouldComponentUpdate() {

  },

  componentDidUpdate() {

  },

  handleSomeThing() {

  },

  handleSomeThingElse() {

  },

  render() {
    const {
      error
      isLoading,
      data
    } = this.props;

    const {
      showDetails
    } = this.state;

    if (error) {
      return <div>Error: {error}</div>;
    }

    if (loading) {
      return <div>loading..</div>;
    }

    return (
      <div>
        <p>Success {data.info}!</p>
        {showDetails && <p>{data.details}</p>}
      </div>
    );
  }
});

module.exports = MyComponent;