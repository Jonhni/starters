"use strict";
const express = require('express');
const app = express();
const server = require('http').createServer(app);
var path = require('path');
const io = require('socket.io')(server);
var users = require('./users.json')
app.use(express.static(path.join(__dirname, 'public')));

// io.on('connection', (client) => {
//   console.log(`Client connected`);
//   client.on('disconnect', ()=>{
//       console.log('Client disconnected')
//   });
//   client.on('message', function(data){
//   });
// });
app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

// app.get('/users', (req,res)=>{
//    res.send(users);
// });
server.listen(8080, () => {
  console.log('Server running on 8080');
});
