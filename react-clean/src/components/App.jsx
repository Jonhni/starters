const React = require('react');
require('../styles/style.scss');

const App = React.createClass({
  render() {
    return (
      <div className="superStyles">
        <h1>My Amazing React App!</h1>
      </div>
    );
  }
});
module.exports = App;
